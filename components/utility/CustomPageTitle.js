import React from 'react';
import { ComponentTitleWrapper } from './CustomPageTitle.styles';

const CustomPageTitle = props => <ComponentTitleWrapper className="isoComponentTitle">
  {props.children}
</ComponentTitleWrapper>;

export default CustomPageTitle;