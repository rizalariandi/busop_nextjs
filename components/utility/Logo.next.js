import React from "react";
import Link from "next/link";
import siteConfig from "@iso/config/site.config";
import logo from "@iso/assets/images/icon/logo.svg";

export default function LogoNext({ collapsed }) {
  return (
    <div className="isoLogoWrapper">
      {collapsed ? (
        <div>
          <h3>
            <Link href="/dashboard">
              <a>
                <img src={logo} alt="bucket" />
              </a>
            </Link>
          </h3>
        </div>
      ) : (
        <h3>
          <Link href="/dashboard">
            <a>
              <img src={logo} alt="bucket" />
            </a>
          </Link>
        </h3>
      )}
    </div>
  );
}
