import styled from 'styled-components';
import { palette } from 'styled-theme';
import WithDirection from '@iso/lib/helpers/rtl';
const WDComponentTitleWrapper = styled.h1`
    font-family: 'Roboto', sans-serif;
    font-size: 24pt;
    font-weight: bold;
    color: black;
    padding-left: 20px;
    padding-top:10px;
    padding-bottom:10px;

    

  

 
`;

const ComponentTitleWrapper = WithDirection(WDComponentTitleWrapper);
export { ComponentTitleWrapper };