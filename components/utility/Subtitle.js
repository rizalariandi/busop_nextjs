import React from 'react';
import { ComponentTitleWrapper } from './Subtitle.style';

const Subtitle = props => <ComponentTitleWrapper className="isoComponentTitle">
  {props.children}
</ComponentTitleWrapper>;

export default Subtitle;