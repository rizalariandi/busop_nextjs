import React from "react";
import Head from "next/head";
import dynamic from "next/dynamic";
import { useDispatch, useSelector } from "react-redux";
import Actions from "@iso/redux/themeSwitcher/actions";
import { Row, Col } from "antd";
import basicStyle from "@iso/assets/styles/constants";
import filter from "@iso/assets/images/icon/filter.svg";
import SingleProgressWidget from "@iso/containers/Widgets/Progress/ProgressSingle";
import BtnFilter from "@iso/containers/Forms/Progress/btnfilter.style";
import IntlMessages from "@iso/components/utility/intlMessages";
import ReportsWidget from "@iso/containers/Widgets/Report/ReportWidget";

const Box = dynamic(() => import("@iso/components/utility/BusopUtil/box"));
const LayoutWrapper = dynamic(() =>
  import("@iso/components/utility/layoutWrapper")
);
const ContentHolder = dynamic(() =>
  import("@iso/components/utility/contentHolder")
);
const { switchActivation, changeTheme } = Actions;

const Bar = dynamic(() =>
  import("@iso/containers/Charts/BusopChart/Components/Bar/Bar")
);
const HorizontalBar = dynamic(() =>
  import(
    "@iso/containers/Charts/BusopChart/Components/HorizontalBar/HorizontalBar"
  )
);
const Doughnut = dynamic(() =>
  import("@iso/containers/Charts/BusopChart/Components/Doughnut/Doughnut")
);
const CustomPageTitle = dynamic(() =>
  import("@iso/components/utility/CustomPageTitle")
);
const SIGNLE_PROGRESS_WIDGET = [
  {
    label: "widget.singleprogresswidget5.label",
    percent: 70,
    barHeight: 5,
    status: "active",
    info: true,
  },
  {
    label: "widget.singleprogresswidget6.label",
    percent: 80,
    barHeight: 5,
    status: "active",
    info: true,
  },
  {
    label: "widget.singleprogresswidget7.label",
    percent: 40,
    barHeight: 5,
    status: "active",
    info: true,
  },
  {
    label: "widget.singleprogresswidget8.label",
    percent: 60,
    barHeight: 5,
    status: "active",
    info: true,
  },
  {
    label: "widget.singleprogresswidget9.label",
    percent: 90,
    barHeight: 5,
    status: "active",
    info: true,
  },
];

import { withAuthSync } from "../../authentication/auth.utils";
import { Dashboard } from "uppy";
import DashboardLayout from "../../containers/DashboardLayout/DashboardLayout";

import CircleIcon from "../../styled/CircleLegendary.style";
export default withAuthSync(() => (
  <>
    <Head>
      <title>Overview</title>
    </Head>
    <DashboardLayout>
      <ReactChart2 />
    </DashboardLayout>
    {/* <ReactChart2 /> */}
  </>
));

function ReactChart2() {
  const dispatch = useDispatch();
  const { rowStyle, colStyle, gutter } = basicStyle;
  return (
    <LayoutWrapper className="isoMapPage">
      <CustomPageTitle>Overview</CustomPageTitle>
      <Row style={rowStyle} gutter={gutter} justify="end">
        <BtnFilter>
          <button
            type="primary"
            className="btn"
            onClick={() => {
              dispatch(switchActivation());
            }}
          >
            <img src={filter} alt="bucket" />
          </button>
        </BtnFilter>
      </Row>
      <Row style={rowStyle} gutter={gutter} justify="start">
        <Col md={12} xs={24} style={colStyle}>
          <Box title="List Category">
            <ContentHolder>
            {SIGNLE_PROGRESS_WIDGET.map((widget, idx) => (
                  <SingleProgressWidget
                    key={idx}
                    label={<IntlMessages id={widget.label} />}
                    percent={widget.percent}
                    barHeight={widget.barHeight}
                    status={widget.status}
                    info={widget.info} // Boolean: true, false
                  />
                ))}
            </ContentHolder>
          </Box>
        </Col>
        <Col md={12} xs={24} style={colStyle}>
          <Box>
            <ContentHolder>
              <Bar />
            </ContentHolder>
          </Box>
        </Col>
      </Row>
      <Row style={rowStyle} gutter={gutter} justify="start">
        <Col md={24} xs={24} style={colStyle}>
          <Box>
            <ContentHolder>
              <Row style={rowStyle} gutter={gutter} justify="start">
                <Col md={10} xs={24} style={colStyle}>
                  <Doughnut />
                  <h1 className="total-ticket website-version">TOTAL TICKET : 30000</h1>
                  <h1 className="total-ticket mobile-version">TOTAL TICKET : 30000</h1>
                </Col>
                <Col md={14} xs={24} style={colStyle}>
                  <Row style={rowStyle} gutter={gutter} justify="start">
                    <Col md={8} xs={8} style={colStyle}>
                      <Row style={rowStyle} gutter={gutter} justify="start">
                        <Col md={4} xs={4} style={colStyle}>
                          <CircleIcon></CircleIcon>
                        </Col>
                        <Col md={12} xs={12} style={colStyle}>
                          <h3 className="legendarylabel">AMKA</h3>
                        </Col>
                        <Col md={8} xs={8} style={colStyle}>
                          <h3 className="legendarycount">300</h3>
                        </Col>
                      </Row>
                    </Col>
                    <Col md={8} xs={8} style={colStyle}>
                      <Row style={rowStyle} gutter={gutter} justify="start">
                        <Col md={4} xs={4} style={colStyle}>
                          <CircleIcon></CircleIcon>
                        </Col>
                        <Col md={12} xs={12} style={colStyle}>
                          <h3 className="legendarylabel">AMKA</h3>
                        </Col>
                        <Col md={8} xs={8} style={colStyle}>
                          <h3 className="legendarycount">300</h3>
                        </Col>
                      </Row>
                    </Col>
                    <Col md={8} xs={8} style={colStyle}>
                      <Row style={rowStyle} gutter={gutter} justify="start">
                        <Col md={4} xs={4} style={colStyle}>
                          <CircleIcon></CircleIcon>
                        </Col>
                        <Col md={12} xs={12} style={colStyle}>
                          <h3 className="legendarylabel">AMKA</h3>
                        </Col>
                        <Col md={8} xs={8} style={colStyle}>
                          <h3 className="legendarycount">300</h3>
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                  <Row style={rowStyle} gutter={gutter} justify="start">
                    <Col md={8} xs={8} style={colStyle}>
                      <Row style={rowStyle} gutter={gutter} justify="start">
                        <Col md={4} xs={4} style={colStyle}>
                          <CircleIcon></CircleIcon>
                        </Col>
                        <Col md={12} xs={12} style={colStyle}>
                          <h3 className="legendarylabel">AMKA</h3>
                        </Col>
                        <Col md={8} xs={8} style={colStyle}>
                          <h3 className="legendarycount">300</h3>
                        </Col>
                      </Row>
                    </Col>
                    <Col md={8} xs={8} style={colStyle}>
                      <Row style={rowStyle} gutter={gutter} justify="start">
                        <Col md={4} xs={4} style={colStyle}>
                          <CircleIcon></CircleIcon>
                        </Col>
                        <Col md={12} xs={12} style={colStyle}>
                          <h3 className="legendarylabel">AMKA</h3>
                        </Col>
                        <Col md={8} xs={8} style={colStyle}>
                          <h3 className="legendarycount">300</h3>
                        </Col>
                      </Row>
                    </Col>
                    <Col md={8} xs={8} style={colStyle}>
                      <Row style={rowStyle} gutter={gutter} justify="start">
                        <Col md={4} xs={4} style={colStyle}>
                          <CircleIcon></CircleIcon>
                        </Col>
                        <Col md={12} xs={12} style={colStyle}>
                          <h3 className="legendarylabel">AMKA</h3>
                        </Col>
                        <Col md={8} xs={8} style={colStyle}>
                          <h3 className="legendarycount">300</h3>
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                  <Row style={rowStyle} gutter={gutter} justify="start">
                    <Col md={8} xs={8} style={colStyle}>
                      <Row style={rowStyle} gutter={gutter} justify="start">
                        <Col md={4} xs={4} style={colStyle}>
                          <CircleIcon></CircleIcon>
                        </Col>
                        <Col md={12} xs={12} style={colStyle}>
                          <h3 className="legendarylabel">AMKA</h3>
                        </Col>
                        <Col md={8} xs={8} style={colStyle}>
                          <h3 className="legendarycount">300</h3>
                        </Col>
                      </Row>
                    </Col>
                    <Col md={8} xs={8} style={colStyle}>
                      <Row style={rowStyle} gutter={gutter} justify="start">
                        <Col md={4} xs={4} style={colStyle}>
                          <CircleIcon></CircleIcon>
                        </Col>
                        <Col md={12} xs={12} style={colStyle}>
                          <h3 className="legendarylabel">AMKA</h3>
                        </Col>
                        <Col md={8} xs={8} style={colStyle}>
                          <h3 className="legendarycount">300</h3>
                        </Col>
                      </Row>
                    </Col>
                    <Col md={8} xs={8} style={colStyle}>
                      <Row style={rowStyle} gutter={gutter} justify="start">
                        <Col md={4} xs={4} style={colStyle}>
                          <CircleIcon></CircleIcon>
                        </Col>
                        <Col md={12} xs={12} style={colStyle}>
                          <h3 className="legendarylabel">AMKA</h3>
                        </Col>
                        <Col md={8} xs={8} style={colStyle}>
                          <h3 className="legendarycount">300</h3>
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                  <Row style={rowStyle} gutter={gutter} justify="start">
                    <Col md={8} xs={8} style={colStyle}>
                      <Row style={rowStyle} gutter={gutter} justify="start">
                        <Col md={4} xs={4} style={colStyle}>
                          <CircleIcon></CircleIcon>
                        </Col>
                        <Col md={12} xs={12} style={colStyle}>
                          <h3 className="legendarylabel">AMKA</h3>
                        </Col>
                        <Col md={8} xs={8} style={colStyle}>
                          <h3 className="legendarycount">300</h3>
                        </Col>
                      </Row>
                    </Col>
                    <Col md={8} xs={8} style={colStyle}>
                      <Row style={rowStyle} gutter={gutter} justify="start">
                        <Col md={4} xs={4} style={colStyle}>
                          <CircleIcon></CircleIcon>
                        </Col>
                        <Col md={12} xs={12} style={colStyle}>
                          <h3 className="legendarylabel">AMKA</h3>
                        </Col>
                        <Col md={8} xs={8} style={colStyle}>
                          <h3 className="legendarycount">300</h3>
                        </Col>
                      </Row>
                    </Col>
                    <Col md={8} xs={8} style={colStyle}>
                      <Row style={rowStyle} gutter={gutter} justify="start">
                        <Col md={4} xs={4} style={colStyle}>
                          <CircleIcon></CircleIcon>
                        </Col>
                        <Col md={12} xs={12} style={colStyle}>
                          <h3 className="legendarylabel">AMKA</h3>
                        </Col>
                        <Col md={8} xs={8} style={colStyle}>
                          <h3 className="legendarycount">300</h3>
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </ContentHolder>
          </Box>
        </Col>
      </Row>
    </LayoutWrapper>
  );
}
