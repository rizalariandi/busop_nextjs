import Head from "next/head";
import Summary from "@iso/containers/Summary/Summary";
import { withAuthSync } from "../../authentication/auth.utils";
import DashboardLayout from "../../containers/DashboardLayout/DashboardLayout";
export default withAuthSync(() => (
  <>
    <Head>
      <title>Summary</title>
    </Head>
    <DashboardLayout>
      <Summary />
    </DashboardLayout>
  </>
));
