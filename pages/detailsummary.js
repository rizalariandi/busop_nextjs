import React from 'react';
import Link from 'next/link';
import Head from 'next/head';
import { useDispatch } from 'react-redux';
import { useRouter } from 'next/router';

import LoginStyleWrapper from '../styled/LoginPage.style';

import { Row, Col } from 'antd';
import basicStyle from '@iso/assets/styles/constants';
import DetailSumaryWrapper from '../styled/DetailSumaryWrapper.styles';
import DetailSumaryCard from '../styled/DetailSumaryCard.style';
import Box from '../styled/Box.style';
import iconphone from '@iso/assets/images/phone-call.svg';
import DashboardLayout from '../containers/DashboardLayout/DashboardLayout';

export default function LoginPage(props) {
    const { rowStyle, colStyle, gutter } = basicStyle;
    return (
      <>
        <Head>
          <title>Detail Sumary</title>
        </Head>
        <DashboardLayout>
          <DetailSumaryWrapper >
              <Row style={rowStyle} gutter={gutter} justify="start" >
                  <Col md={24} sm={24} xs={24} >
                    <h1 className="Perusahaan">Amka</h1>
                  </Col>
              </Row>
              <Row style={rowStyle} gutter={gutter} justify="start" >
                  <Col md={24} sm={24} xs={24} >
                    <DetailSumaryCard>
                      <Row style={rowStyle} gutter={gutter} justify="start" >
                        <Col md={1} sm={0} xs={0} />
                        <Col md={3} sm={6} xs={8} >
                          <Box>
                            <center>
                              <p className="icon">A</p>
                            </center>
                          </Box>
                        </Col>
                        <Col md={19} sm={18} xs={16} >
                          <Row style={rowStyle} gutter={gutter} justify="start" >
                              <Col md={24} sm={24} xs={24} >
                                <h3 className="card-title">[AMKA] Bug di travel request <span className="id">#29548594</span> </h3>
                              </Col>
                          </Row>
                          <Row style={rowStyle} gutter={gutter} justify="start" >
                              <Col md={24} sm={24} xs={24}>
                                <Row style={rowStyle} gutter={gutter} justify="start" >
                                    <Col md={1} sm={1} xs={1} >
                                      <img src={iconphone} alt="" className="icon-phone" />
                                    </Col>
                                    <Col md={23} sm={23} xs={23} >
                                      <p className="card-subtitle">From : Ade Irma([AMKA]) . Close 2 Hour ago . Group:-- . Agent:Technical Support . Status:Close . Priority:Low</p>
                                    </Col>
                                </Row>
                                
                                
                              </Col>
                          </Row>
                        </Col>
                      </Row>
                    </DetailSumaryCard>
                  </Col>
              </Row>
              <Row style={rowStyle} gutter={gutter} justify="start" >
                  <Col md={24} sm={24} xs={24} >
                    <DetailSumaryCard>
                      <Row style={rowStyle} gutter={gutter} justify="start" >
                        <Col md={1} sm={0} xs={0} />
                        <Col md={3} sm={6} xs={8} >
                          <Box>
                            <center>
                              <p className="icon">A</p>
                            </center>
                          </Box>
                        </Col>
                        <Col md={19} sm={18} xs={16} >
                          <Row style={rowStyle} gutter={gutter} justify="start" >
                              <Col md={24} sm={24} xs={24} >
                                <h3 className="card-title">[AMKA] Bug di travel request <span className="id">#29548594</span> </h3>
                              </Col>
                          </Row>
                          <Row style={rowStyle} gutter={gutter} justify="start" >
                              <Col md={24} sm={24} xs={24}>
                                <Row style={rowStyle} gutter={gutter} justify="start" >
                                    <Col md={1} sm={1} xs={1} >
                                      <img src={iconphone} alt="" className="icon-phone" />
                                    </Col>
                                    <Col md={23} sm={23} xs={23} >
                                      <p className="card-subtitle">From : Ade Irma([AMKA]) . Close 2 Hour ago . Group:-- . Agent:Technical Support . Status:Close . Priority:Low</p>
                                    </Col>
                                </Row>
                                
                                
                              </Col>
                          </Row>
                        </Col>
                      </Row>
                    </DetailSumaryCard>
                  </Col>
              </Row>
              <Row style={rowStyle} gutter={gutter} justify="start" >
                  <Col md={24} sm={24} xs={24} >
                    <DetailSumaryCard>
                      <Row style={rowStyle} gutter={gutter} justify="start" >
                        <Col md={1} sm={0} xs={0} />
                        <Col md={3} sm={6} xs={8} >
                          <Box>
                            <center>
                              <p className="icon">A</p>
                            </center>
                          </Box>
                        </Col>
                        <Col md={19} sm={18} xs={16} >
                          <Row style={rowStyle} gutter={gutter} justify="start" >
                              <Col md={24} sm={24} xs={24} >
                                <h3 className="card-title">[AMKA] Bug di travel request <span className="id">#29548594</span> </h3>
                              </Col>
                          </Row>
                          <Row style={rowStyle} gutter={gutter} justify="start" >
                              <Col md={24} sm={24} xs={24}>
                                <Row style={rowStyle} gutter={gutter} justify="start" >
                                    <Col md={1} sm={1} xs={1} >
                                      <img src={iconphone} alt="" className="icon-phone" />
                                    </Col>
                                    <Col md={23} sm={23} xs={23} >
                                      <p className="card-subtitle">From : Ade Irma([AMKA]) . Close 2 Hour ago . Group:-- . Agent:Technical Support . Status:Close . Priority:Low</p>
                                    </Col>
                                </Row>
                                
                                
                              </Col>
                          </Row>
                        </Col>
                      </Row>
                    </DetailSumaryCard>
                  </Col>
              </Row>
              <Row style={rowStyle} gutter={gutter} justify="start" >
                  <Col md={24} sm={24} xs={24} >
                    <DetailSumaryCard>
                      <Row style={rowStyle} gutter={gutter} justify="start" >
                        <Col md={1} sm={0} xs={0} />
                        <Col md={3} sm={6} xs={8} >
                          <Box>
                            <center>
                              <p className="icon">A</p>
                            </center>
                          </Box>
                        </Col>
                        <Col md={19} sm={18} xs={16} >
                          <Row style={rowStyle} gutter={gutter} justify="start" >
                              <Col md={24} sm={24} xs={24} >
                                <h3 className="card-title">[AMKA] Bug di travel request <span className="id">#29548594</span> </h3>
                              </Col>
                          </Row>
                          <Row style={rowStyle} gutter={gutter} justify="start" >
                              <Col md={24} sm={24} xs={24}>
                                <Row style={rowStyle} gutter={gutter} justify="start" >
                                    <Col md={1} sm={1} xs={1} >
                                      <img src={iconphone} alt="" className="icon-phone" />
                                    </Col>
                                    <Col md={23} sm={23} xs={23} >
                                      <p className="card-subtitle">From : Ade Irma([AMKA]) . Close 2 Hour ago . Group:-- . Agent:Technical Support . Status:Close . Priority:Low</p>
                                    </Col>
                                </Row>
                                
                                
                              </Col>
                          </Row>
                        </Col>
                      </Row>
                    </DetailSumaryCard>
                  </Col>
              </Row>
              <Row style={rowStyle} gutter={gutter} justify="start" >
                  <Col md={24} sm={24} xs={24} >
                    <DetailSumaryCard>
                      <Row style={rowStyle} gutter={gutter} justify="start" >
                        <Col md={1} sm={0} xs={0} />
                        <Col md={3} sm={6} xs={8} >
                          <Box>
                            <center>
                              <p className="icon">A</p>
                            </center>
                          </Box>
                        </Col>
                        <Col md={19} sm={18} xs={16} >
                          <Row style={rowStyle} gutter={gutter} justify="start" >
                              <Col md={24} sm={24} xs={24} >
                                <h3 className="card-title">[AMKA] Bug di travel request <span className="id">#29548594</span> </h3>
                              </Col>
                          </Row>
                          <Row style={rowStyle} gutter={gutter} justify="start" >
                              <Col md={24} sm={24} xs={24}>
                                <Row style={rowStyle} gutter={gutter} justify="start" >
                                    <Col md={1} sm={1} xs={1} >
                                      <img src={iconphone} alt="" className="icon-phone" />
                                    </Col>
                                    <Col md={23} sm={23} xs={23} >
                                      <p className="card-subtitle">From : Ade Irma([AMKA]) . Close 2 Hour ago . Group:-- . Agent:Technical Support . Status:Close . Priority:Low</p>
                                    </Col>
                                </Row>
                                
                                
                              </Col>
                          </Row>
                        </Col>
                      </Row>
                    </DetailSumaryCard>
                  </Col>
              </Row>
              <Row style={rowStyle} gutter={gutter} justify="start" >
                  <Col md={24} sm={24} xs={24} >
                    <DetailSumaryCard>
                      <Row style={rowStyle} gutter={gutter} justify="start" >
                        <Col md={1} sm={0} xs={0} />
                        <Col md={3} sm={6} xs={8} >
                          <Box>
                            <center>
                              <p className="icon">A</p>
                            </center>
                          </Box>
                        </Col>
                        <Col md={19} sm={18} xs={16} >
                          <Row style={rowStyle} gutter={gutter} justify="start" >
                              <Col md={24} sm={24} xs={24} >
                                <h3 className="card-title">[AMKA] Bug di travel request <span className="id">#29548594</span> </h3>
                              </Col>
                          </Row>
                          <Row style={rowStyle} gutter={gutter} justify="start" >
                              <Col md={24} sm={24} xs={24}>
                                <Row style={rowStyle} gutter={gutter} justify="start" >
                                    <Col md={1} sm={1} xs={1} >
                                      <img src={iconphone} alt="" className="icon-phone" />
                                    </Col>
                                    <Col md={23} sm={23} xs={23} >
                                      <p className="card-subtitle">From : Ade Irma([AMKA]) . Close 2 Hour ago . Group:-- . Agent:Technical Support . Status:Close . Priority:Low</p>
                                    </Col>
                                </Row>
                                
                                
                              </Col>
                          </Row>
                        </Col>
                      </Row>
                    </DetailSumaryCard>
                  </Col>
              </Row>
              
          </DetailSumaryWrapper>
        </DashboardLayout>
        
       
      </>
    );
  }

  