import React from "react";
import Cards from "@iso/components/uielements/card";
import styled from "styled-components";
import { palette } from "styled-theme";
import {
  transition,
  borderRadius,
  boxShadow,
} from "@iso/lib/helpers/style_utils";

const BtnFilter = styled.div`
  .btn {
    width: 150px;
    height: 50px;
    background-color: transparent;
    border: none;
    ${transition()};
  }
  .btn:active {
    margin-left: 280px;
  }
`;
export default BtnFilter;
