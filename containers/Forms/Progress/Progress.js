import React, { useState, lazy, useRef, useEffect } from "react";
import { Row, Col } from "antd";
import PageHeader from "@iso/components/utility/pageHeader";
import { CardIcon } from "@iso/containers/Summary/Summary.style";
import filter from "@iso/assets/images/icon/filter.svg";
import { useDispatch, useSelector } from "react-redux";
import Switcher from "@iso/components/ThemeSwitcher/ThemeSwitcher";
import LayoutWrapper from "@iso/components/utility/layoutWrapper";
import ContentHolder from "@iso/components/utility/contentHolder";
import Head from "next/head";
import Actions from "@iso/redux/themeSwitcher/actions";
import basicStyle from "@iso/assets/styles/constants";
import BtnFilter from "./btnfilter.style";
import Card, { ThemeSwitcherStyle } from "./Progress.style.js";
import Box from "@iso/components/utility/box";
import DatePick from "@iso/containers/ThemeSwitcher/Datepicker";
import close from "@iso/assets/images/icon/07-icon.svg";
import OutsideClickHandler from "react-outside-click-handler";

// const Line = lazy(() => import("./Components/Line/Line"));
import Line from "./Line";
const { switchActivation, changeTheme } = Actions;

export default function () {
  const { isActivated, topbarTheme, sidebarTheme, layoutTheme } = useSelector(
    (state) => state.ThemeSwitcher
  );
  const { rowStyle, colStyle, gutter } = basicStyle;
  const dispatch = useDispatch();

  const [data, setData] = useState(null)
  const [isLoading, setLoading] = useState(false)

  useEffect(() => {
    setLoading(true)
    fetch('http://180.250.145.106:16888/apis/api/v1/obu/current_position', {method: 'GET'})
      .then((res) => res.json())
      .then((data) => {
        setData(data);
        setLoading(false);
        console.log(data);
      })
  }, [])

  return (
    <LayoutWrapper>
      <PageHeader>Progress</PageHeader>
      <Row style={rowStyle} gutter={gutter} justify="end">
        <BtnFilter>
          <button
            type="primary"
            className="btn"
            id="btn"
            onClick={() => {
              dispatch(switchActivation());
            }}
          >
            <img src={filter} alt="bucket" />
          </button>
        </BtnFilter>
      </Row>
      <Row style={rowStyle} gutter={gutter} justify="start">
        <Col span={24} style={colStyle}>
          <ContentHolder style={{ overflow: "hidden" }}>
            <Row>
              <Col md={4} sm={5} xs={15} style={{ padding: "20px 8px" }}>
                <Card>
                  <h1>Ticket Open</h1>
                  <p>1</p>
                </Card>
              </Col>
              <Col md={4} sm={5} xs={15} style={{ padding: "20px 8px" }}>
                <Card>
                  <h1>Progress Tiket</h1>
                  <p>7</p>
                </Card>
              </Col>
              <Col md={4} sm={5} xs={15} style={{ padding: "20px 8px" }}>
                <Card>
                  <h1>Close Tiket</h1>
                  <p>1185</p>
                </Card>
              </Col>
              <Col md={6} sm={5} xs={15} style={{ padding: "20px 8px" }}>
                <Card>
                  <h1>Percentage First Response</h1>
                  <p>99,50%</p>
                </Card>
              </Col>
              <Col md={6} sm={5} xs={15} style={{ padding: "20px 8px" }}>
                <Card>
                  <h1>Percentage Resolution</h1>
                  <p>100%</p>
                </Card>
              </Col>
            </Row>
          </ContentHolder>
        </Col>
      </Row>
      <Col xs={24} style={colStyle}>
        <Box title="Line">
          <ContentHolder>
            <Line />
          </ContentHolder>
        </Box>
      </Col>
    </LayoutWrapper>
  );
}
