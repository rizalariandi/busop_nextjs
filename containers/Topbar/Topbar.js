import React, { Component } from "react";
import { connect } from "react-redux";
import { Layout } from "antd";
import appActions from "@iso/redux/app/actions";
import TopbarNotification from "./TopbarNotification";
import TopbarMessage from "./TopbarMessage";
import TopbarSearch from "./TopbarSearch";
import TopbarUser from "./TopbarUser";
import TopbarAddtoCart from "./TopbarAddToCart";
import TopbarWrapper from "./Topbar.styles";
import { TopbarMenuIcon } from "@iso/config/icon.config";
import dynamic from "next/dynamic";

const { Header } = Layout;
const { toggleCollapsed } = appActions;
const today = new Date();
class Topbar extends Component {
  render() {
    const { toggleCollapsed, url, customizedTheme, locale } = this.props;
    const collapsed = this.props.collapsed && !this.props.closeDrawer;
    const styling = {
      background: customizedTheme.backgroundColor,
      position: "fixed",
      width: "100%",
      height: 70,
    };
    return (
      <TopbarWrapper>
        <Header
          style={styling}
          className={
            collapsed ? "isomorphicTopbar collapsed" : "isomorphicTopbar"
          }
        >
          <div className="isoLeft">
            {/* <button
              className={
                collapsed ? "triggerBtn menuCollapsed" : "triggerBtn menuOpen"
              }
              style={{ color: customizedTheme.textColor }}
              onClick={toggleCollapsed}
            >
              <TopbarMenuIcon size={24} color={customizedTheme.textColor} />
            </button> */}
            <div className="title">
              <h3>Single Dashboard Business Support</h3>
            </div>
          </div>

          <ul className="isoRight">
            <li className="isoSearch">
              <input
                className="search"
                type="text"
                id="search"
                placeholder="Search"
              />
            </li>

            <li>
              <p className="date">
                {" "}
                {today.getDate()}/{today.getMonth() + 1},{today.getHours()}:
                {today.getMinutes()}:{today.getSeconds()}
              </p>
            </li>

            {/* <li
              onClick={() => this.setState({ selectedItem: "message" })}
              className="isoMsg"
            >
              <TopbarMessage locale={locale} />
            </li>
            <li
              onClick={() => this.setState({ selectedItem: "addToCart" })}
              className="isoCart"
            >
              <TopbarAddtoCart url={url} locale={locale} />
            </li>

            <li
              onClick={() => this.setState({ selectedItem: "user" })}
              className="isoUser"
            >
              <TopbarUser locale={locale} />
            </li> */}
          </ul>
        </Header>
      </TopbarWrapper>
    );
  }
}

export default connect(
  (state) => ({
    ...state.App,
    locale: state.LanguageSwitcher.language.locale,
    customizedTheme: state.ThemeSwitcher.topbarTheme,
  }),
  { toggleCollapsed }
)(Topbar);
