import React, { Component, lazy } from "react";
import { Row, Col } from "antd";
import PageHeader from "@iso/components/utility/pageHeader";
import { CardIcon } from "./Summary.style";
import up from "@iso/assets/images/icon/up.svg";
import down from "@iso/assets/images/icon/down.svg";
import LayoutWrapper from "@iso/components/utility/layoutWrapper";
import ContentHolder from "@iso/components/utility/contentHolder";
import IntlMessages from "@iso/components/utility/intlMessages";
import basicStyle from "@iso/assets/styles/constants";
import Card from "./Summary.style.js";
import Link from 'next/link';
// const Line = lazy(() => import("./Components/Line/Line"));

export default function () {
  const { rowStyle, colStyle, gutter } = basicStyle;
  return (
    <LayoutWrapper>
      <PageHeader>Summary</PageHeader>
      <Row style={rowStyle} gutter={gutter} justify="start">
        <Col span={24} style={colStyle}>
          <ContentHolder style={{ overflow: "hidden" }}>
            <Row>
              <Col md={6} sm={5} xs={15} style={{ padding: "20px 8px" }}>
                <Card title="AMKA">
                  <div className="text-content">
                    <p className="text">0</p>
                    <p className="text">0</p>
                    <p className="text">262</p>
                  </div>
                  <div className="status-text">
                    <p className="up-text">
                      <CardIcon className="icon" src={up} />
                      open
                    </p>
                    <p className="onprogress">on progress</p>
                    <p className="closed">
                      <CardIcon className="icon" src={down} />
                      closed
                    </p>
                  </div>
                  <div className="btn">
                    <Link href="/detailsummary">
                      <button type="button" class="btn btn-link">
                        Lihat detail
                      </button>
                    </Link>
                    
                  </div>
                </Card>
              </Col>
              <Col md={6} sm={5} xs={15} style={{ padding: "20px 8px" }}>
                <Card title="BRI">
                  <div className="text-content">
                    <p className="text">0</p>
                    <p className="text">0</p>
                    <p className="text">262</p>
                  </div>
                  <div className="status-text">
                    <p className="up-text">
                      <CardIcon className="icon" src={up} />
                      open
                    </p>
                    <p className="onprogress">on progress</p>
                    <p className="closed">
                      <CardIcon className="icon" src={down} />
                      closed
                    </p>
                  </div>
                  <div className="btn">
                    <div className="btn">
                      <Link href="/detailsummary">
                        <button type="button" class="btn btn-link">
                          Lihat detail
                        </button>
                      </Link>
                      
                    </div>
                  </div>
                </Card>
              </Col>
              <Col md={6} sm={5} xs={15} style={{ padding: "20px 8px" }}>
                <Card title="BPJS">
                  <div className="text-content">
                    <p className="text">0</p>
                    <p className="text">0</p>
                    <p className="text">262</p>
                  </div>
                  <div className="status-text">
                    <p className="up-text">
                      <CardIcon className="icon" src={up} />
                      open
                    </p>
                    <p className="onprogress">on progress</p>
                    <p className="closed">
                      <CardIcon className="icon" src={down} />
                      closed
                    </p>
                  </div>
                  <div className="btn">
                    <Link href="/detailsummary">
                      <button type="button" class="btn btn-link">
                        Lihat detail
                      </button>
                    </Link>
                    
                  </div>
                </Card>
              </Col>
              <Col md={6} sm={5} xs={15} style={{ padding: "20px 8px" }}>
                <Card title="CAZBOND">
                  <div className="text-content">
                    <p className="text">0</p>
                    <p className="text">0</p>
                    <p className="text">262</p>
                  </div>
                  <div className="status-text">
                    <p className="up-text">
                      <CardIcon className="icon" src={up} />
                      open
                    </p>
                    <p className="onprogress">on progress</p>
                    <p className="closed">
                      <CardIcon className="icon" src={down} />
                      closed
                    </p>
                  </div>
                  <div className="btn">
                    <Link href="/detailsummary">
                      <button type="button" class="btn btn-link">
                        Lihat detail
                      </button>
                    </Link>
                    
                  </div>
                </Card>
              </Col>
            </Row>
            <Row>
              <Col md={6} sm={5} xs={15} style={{ padding: "20px 8px" }}>
                <Card title="CORCOM">
                  <div className="text-content">
                    <p className="text">0</p>
                    <p className="text">0</p>
                    <p className="text">262</p>
                  </div>
                  <div className="status-text">
                    <p className="up-text">
                      <CardIcon className="icon" src={up} />
                      open
                    </p>
                    <p className="onprogress">on progress</p>
                    <p className="closed">
                      <CardIcon className="icon" src={down} />
                      closed
                    </p>
                  </div>
                  <div className="btn">
                    <Link href="/detailsummary">
                      <button type="button" class="btn btn-link">
                        Lihat detail
                      </button>
                    </Link>
                    
                  </div>
                </Card>
              </Col>
              <Col md={6} sm={5} xs={15} style={{ padding: "20px 8px" }}>
                <Card title="E-OFFICE PGD">
                  <div className="text-content">
                    <p className="text">0</p>
                    <p className="text">0</p>
                    <p className="text">262</p>
                  </div>
                  <div className="status-text">
                    <p className="up-text">
                      <CardIcon className="icon" src={up} />
                      open
                    </p>
                    <p className="onprogress">on progress</p>
                    <p className="closed">
                      <CardIcon className="icon" src={down} />
                      closed
                    </p>
                  </div>
                  <div className="btn">
                    <Link href="/detailsummary">
                      <button type="button" class="btn btn-link">
                        Lihat detail
                      </button>
                    </Link>
                    
                  </div>
                </Card>
              </Col>
              <Col md={6} sm={5} xs={15} style={{ padding: "20px 8px" }}>
                <Card title="GLOBAL">
                  <div className="text-content">
                    <p className="text">0</p>
                    <p className="text">0</p>
                    <p className="text">262</p>
                  </div>
                  <div className="status-text">
                    <p className="up-text">
                      <CardIcon className="icon" src={up} />
                      open
                    </p>
                    <p className="onprogress">on progress</p>
                    <p className="closed">
                      <CardIcon className="icon" src={down} />
                      closed
                    </p>
                  </div>
                  <div className="btn">
                    <Link href="/detailsummary">
                      <button type="button" class="btn btn-link">
                        Lihat detail
                      </button>
                    </Link>
                    
                  </div>
                </Card>
              </Col>
              <Col md={6} sm={5} xs={15} style={{ padding: "20px 8px" }}>
                <Card title="IKONSER">
                  <div className="text-content">
                    <p className="text">0</p>
                    <p className="text">0</p>
                    <p className="text">262</p>
                  </div>
                  <div className="status-text">
                    <p className="up-text">
                      <CardIcon className="icon" src={up} />
                      open
                    </p>
                    <p className="onprogress">on progress</p>
                    <p className="closed">
                      <CardIcon className="icon" src={down} />
                      closed
                    </p>
                  </div>
                  <div className="btn">
                    <Link href="/detailsummary">
                      <button type="button" class="btn btn-link">
                        Lihat detail
                      </button>
                    </Link>
                    
                  </div>
                </Card>
              </Col>
            </Row>
            <Row>
              <Col md={6} sm={5} xs={15} style={{ padding: "20px 8px" }}>
                <Card title="KAI">
                  <div className="text-content">
                    <p className="text">0</p>
                    <p className="text">0</p>
                    <p className="text">262</p>
                  </div>
                  <div className="status-text">
                    <p className="up-text">
                      <CardIcon className="icon" src={up} />
                      open
                    </p>
                    <p className="onprogress">on progress</p>
                    <p className="closed">
                      <CardIcon className="icon" src={down} />
                      closed
                    </p>
                  </div>
                  <div className="btn">
                    <Link href="/detailsummary">
                      <button type="button" class="btn btn-link">
                        Lihat detail
                      </button>
                    </Link>
                    
                  </div>
                </Card>
              </Col>
              <Col md={6} sm={5} xs={15} style={{ padding: "20px 8px" }}>
                <Card title="KEMENTAN">
                  <div className="text-content">
                    <p className="text">0</p>
                    <p className="text">0</p>
                    <p className="text">262</p>
                  </div>
                  <div className="status-text">
                    <p className="up-text">
                      <CardIcon className="icon" src={up} />
                      open
                    </p>
                    <p className="onprogress">on progress</p>
                    <p className="closed">
                      <CardIcon className="icon" src={down} />
                      closed
                    </p>
                  </div>
                  <div className="btn">
                    <Link href="/detailsummary">
                      <button type="button" class="btn btn-link">
                        Lihat detail
                      </button>
                    </Link>
                    
                  </div>
                </Card>
              </Col>
              <Col md={6} sm={5} xs={15} style={{ padding: "20px 8px" }}>
                <Card title="KEMENKES">
                  <div className="text-content">
                    <p className="text">0</p>
                    <p className="text">0</p>
                    <p className="text">262</p>
                  </div>
                  <div className="status-text">
                    <p className="up-text">
                      <CardIcon className="icon" src={up} />
                      open
                    </p>
                    <p className="onprogress">on progress</p>
                    <p className="closed">
                      <CardIcon className="icon" src={down} />
                      closed
                    </p>
                  </div>
                  <div className="btn">
                    <Link href="/detailsummary">
                      <button type="button" class="btn btn-link">
                        Lihat detail
                      </button>
                    </Link>
                    
                  </div>
                </Card>
              </Col>
              <Col md={6} sm={5} xs={15} style={{ padding: "20px 8px" }}>
                <Card title="KIMIA FARMA">
                  <div className="text-content">
                    <p className="text">0</p>
                    <p className="text">0</p>
                    <p className="text">262</p>
                  </div>
                  <div className="status-text">
                    <p className="up-text">
                      <CardIcon className="icon" src={up} />
                      open
                    </p>
                    <p className="onprogress">on progress</p>
                    <p className="closed">
                      <CardIcon className="icon" src={down} />
                      closed
                    </p>
                  </div>
                  <div className="btn">
                    <Link href="/detailsummary">
                      <button type="button" class="btn btn-link">
                        Lihat detail
                      </button>
                    </Link>
                    
                  </div>
                </Card>
              </Col>
            </Row>
            <Row>
              <Col md={6} sm={5} xs={15} style={{ padding: "20px 8px" }}>
                <Card title="PADI UMKM">
                  <div className="text-content">
                    <p className="text">0</p>
                    <p className="text">0</p>
                    <p className="text">262</p>
                  </div>
                  <div className="status-text">
                    <p className="up-text">
                      <CardIcon className="icon" src={up} />
                      open
                    </p>
                    <p className="onprogress">on progress</p>
                    <p className="closed">
                      <CardIcon className="icon" src={down} />
                      closed
                    </p>
                  </div>
                  <div className="btn">
                    <Link href="/detailsummary">
                      <button type="button" class="btn btn-link">
                        Lihat detail
                      </button>
                    </Link>
                    
                  </div>
                </Card>
              </Col>
              <Col md={6} sm={5} xs={15} style={{ padding: "20px 8px" }}>
                <Card title="PEGADAIAN">
                  <div className="text-content">
                    <p className="text">0</p>
                    <p className="text">0</p>
                    <p className="text">262</p>
                  </div>
                  <div className="status-text">
                    <p className="up-text">
                      <CardIcon className="icon" src={up} />
                      open
                    </p>
                    <p className="onprogress">on progress</p>
                    <p className="closed">
                      <CardIcon className="icon" src={down} />
                      closed
                    </p>
                  </div>
                  <div className="btn">
                    <Link href="/detailsummary">
                      <button type="button" class="btn btn-link">
                        Lihat detail
                      </button>
                    </Link>
                    
                  </div>
                </Card>
              </Col>
              <Col md={6} sm={5} xs={15} style={{ padding: "20px 8px" }}>
                <Card title="PERHUTANI">
                  <div className="text-content">
                    <p className="text">0</p>
                    <p className="text">0</p>
                    <p className="text">262</p>
                  </div>
                  <div className="status-text">
                    <p className="up-text">
                      <CardIcon className="icon" src={up} />
                      open
                    </p>
                    <p className="onprogress">on progress</p>
                    <p className="closed">
                      <CardIcon className="icon" src={down} />
                      closed
                    </p>
                  </div>
                  <div className="btn">
                    <Link href="/detailsummary">
                      <button type="button" class="btn btn-link">
                        Lihat detail
                      </button>
                    </Link>
                    
                  </div>
                </Card>
              </Col>
              <Col md={6} sm={5} xs={15} style={{ padding: "20px 8px" }}>
                <Card title="PERTAMINA">
                  <div className="text-content">
                    <p className="text">0</p>
                    <p className="text">0</p>
                    <p className="text">262</p>
                  </div>
                  <div className="status-text">
                    <p className="up-text">
                      <CardIcon className="icon" src={up} />
                      open
                    </p>
                    <p className="onprogress">on progress</p>
                    <p className="closed">
                      <CardIcon className="icon" src={down} />
                      closed
                    </p>
                  </div>
                  <div className="btn">
                    <Link href="/detailsummary">
                      <button type="button" class="btn btn-link">
                        Lihat detail
                      </button>
                    </Link>
                    
                  </div>
                </Card>
              </Col>
            </Row>
          </ContentHolder>
        </Col>
      </Row>
    </LayoutWrapper>
  );
}
