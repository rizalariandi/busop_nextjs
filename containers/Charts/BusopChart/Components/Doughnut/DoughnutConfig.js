const data = {
  labels: ['AMKA', 'BRI', 'BPJS','CAZBOND','CORCOM','KAI','IKONSER','KEMENTAN','KEMENPAR','KEMENDIKBUD','KIMIA FARMA', 'PEGADAIAN','JASA MARGA', 'PERTAMINA'],
  datasets: [
    {
      data: [300, 50, 100,200,300,400,500,600,700,800,900,1000,1100,1200],
      backgroundColor: ['#ff6384', '#48A6F2', '#ffbf00','#F73D93','#8000FF','#F9FFA4','#000000','#FFFF00','#00FFFF','#FF00FF','#00FF00','#997300','#7CAFDD','#FB0000'],
      hoverBackgroundColor: ['#FF6384', '#48A6F2', '#ffbf00','#F73D93','#8000FF','#F9FFA4','#000000','#FFFF00','#00FFFF','#FF00FF','#00FF00','#997300','#7CAFDD','#FB0000'],
    },
  ],
};

export { data };
