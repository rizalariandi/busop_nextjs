const data = {
  labels: ['Problem', 'Incident', 'Feature Request', 'Question', 'Maintanance'],
  datasets: [
    {
      label: 'My First dataset',
      backgroundColor: ['#8000FF','#E182D8','#8000FF','#8000FF','#8000FF'],
      borderColor: ['#8000FF','#E182D8','#8000FF','#8000FF','#8000FF'],
      borderWidth: 1,
      hoverBackgroundColor: ['#8000FF','#E182D8','#8000FF','#8000FF','#8000FF'],
      hoverBorderColor: ['#8000FF','#E182D8','#8000FF','#8000FF','#8000FF'],
      data: [1986, 735, 5232, 471, 3449],
    },
  ],
  options: {
    defaultFontColor: '#788195',
  },
};

export { data };
