const data = {
  labels: ['Low', 'Medium', 'High','Urgent'],
  datasets: [
    {
      label: '# of Votes',
      backgroundColor: ['#0FC100', '#3383CD', '#0BD6C9','#FF0000'],
      borderColor: ['#0FC100', '#3383CD', '#0BD6C9','#FF0000'],
      borderWidth: 1,
      hoverBackgroundColor: ['#0FC100', '#3383CD', '#0BD6C9','#FF0000'],
      hoverBorderColor: ['#0FC100', '#3383CD', '#0BD6C9','#FF0000'],
      data: [7657,2356,1365,485],
    },
  ],
  
  
};

const barSettings = {
  height: 350,
  
};



export { data, barSettings };
