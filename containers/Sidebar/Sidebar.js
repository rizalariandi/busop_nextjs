import React, { useState, useRef, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Modal from "./modal";
import clone from "clone";
import { Layout } from "antd";
import Scrollbars from "@iso/components/utility/customScrollBar";
import Menu from "@iso/components/uielements/menu";
import IntlMessages from "@iso/components/utility/intlMessages";
import Link from "next/link";
import { Row, Col } from "antd";
import appActions from "@iso/redux/app/actions";
import Logo from "@iso/components/utility/Logo.next";
import basicStyle from "@iso/assets/styles/constants";
import SidebarWrapper from "./Sidebar.styles";
import SidebarMenu from "./SidebarMenu";
import SIDEBAR_MENU_OPTIONS from "./sidebar.navigations";
import { SidebarPower, SidebarHelpIcon } from "@iso/config/icon.config";
import authAction from "../../authentication/actions";
import SummaryL from "./summary";
import Help from "./help";
import Overview from "./overview";
import Progress from "./progress";
import iconwa from "@iso/assets/images/icon-wa.png";
import icontele from "@iso/assets/images/icon-tele.png";
import iconmsg from "@iso/assets/images/icon-msg.png";
import { initialize } from "react-ga";
import { set } from "nprogress";
import { Tooltip } from 'antd';

const { logout } = authAction;
const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;
const { Sider } = Layout;
const { rowStyle, colStyle, gutter } = basicStyle;
const { toggleCloseDrawer, changeOpenKeys, changeCurrent, toggleCollapsed } =
  appActions;
export default function Sidebar(props) {
  const [isOpen, setisOpen] = useState(false);
  // const btnRef = useRef();

  // useEffect(effect:() => {

  //   if(e.path[0] !== btnRef.current){
  //     set.Isopen(value:false);
  //   }

  // });

  const { view, openKeys, collapsed, CloseDrawer, height, current } =
    useSelector((state) => state.App);
  const { sidebarTheme } = useSelector((state) => state.ThemeSwitcher);
  const dispatch = useDispatch();
  function handleClick(e) {
    dispatch(changeCurrent([e.key]));

    if (view === "MobileView") {
      setTimeout(() => {
        dispatch(toggleCollapsed());
        // dispatch(toggleOpenDrawer());
      }, 100);
    } else {
      dispatch(toggleCollapsed());
    }
  }
  function onOpenChange(newOpenKeys) {
    const latestOpenKey = newOpenKeys.find(
      (key) => !(openKeys.indexOf(key) > -1)
    );
    const latestCloseKey = openKeys.find(
      (key) => !(newOpenKeys.indexOf(key) > -1)
    );
    let nextOpenKeys = [];
    if (latestOpenKey) {
      nextOpenKeys = getAncestorKeys(latestOpenKey).concat(latestOpenKey);
    }
    if (latestCloseKey) {
      nextOpenKeys = getAncestorKeys(latestCloseKey);
    }
    dispatch(changeOpenKeys(nextOpenKeys));
  }
  const getAncestorKeys = (key) => {
    const map = {
      sub3: ["sub2"],
    };
    return map[key] || [];
  };
  const { logout } = authAction;
  const isCollapsed = collapsed && !CloseDrawer;
  const mode = isCollapsed === true ? "vertical" : "inline";
  // const scrollheight = height;
  const styling = {
    backgroundColor: sidebarTheme.backgroundColor,
  };

  const submenuStyle = {
    backgroundColor: "rgba(0,0,0,0.1)",
    color: sidebarTheme.textColor,
  };
  const submenuColor = {
    color: sidebarTheme.backgroundColor,
  };
  // const onMouseEnter = () => {
  //   if (collapsed && openDrawer === false) {
  //     dispatch(toggleOpenDrawer());
  //   }
  //   return;
  // };
  const onMouseLeave = () => {
    if (collapsed && openDrawer === true) {
      // dispatch(toggleOpenDrawer());
      dispatch(toggleCollapsed());
    }
    return;
  };

  return (
    <SidebarWrapper>
      <Sider
        trigger={null}
        collapsible={true}
        width={70}
        className="isomorphicSidebar"
        // onMouseEnter={onMouseEnter}
        // onMouseLeave={onMouseLeave}
        style={styling}
      >
        <Logo collapsed={isCollapsed} />
        <Scrollbars style={{ height: height - 70 }}>
          <Menu
            onClick={handleClick}
            theme="dark"
            mode={mode}
            // openKeys={isCollapsed ? [] : openKeys}
            selectedKeys={current}
            className="isoDashboardMenu"
            // inlineCollapsed={isCollapsed}
          >
            {SIDEBAR_MENU_OPTIONS.map((option) => (
              <SidebarMenu
                key={option.key}
                item={option}
                submenuColor={submenuColor}
                submenuStyle={submenuStyle}
              />
            ))}
            <div className="top-btn">
              <SubMenu
                title={
                  <div>
                    <Tooltip title="Dashboard Overview" placement="right">
                      <div className="logo1">
                        <Link href="/dashboard">
                          <a className="overview-logo">
                            <Overview />
                          </a>
                        </Link>
                      </div>
                    </Tooltip>
                  </div>
                }
              ></SubMenu>
              <SubMenu
                title={
                  <div>
                    <Tooltip title="Dashboard Summary" placement="right">
                        <div className="logo1">
                          <Link href="/dashboard/Summary">
                            <a className="help-logo">
                              <SummaryL />
                            </a>
                          </Link>
                        </div>
                    </Tooltip>
                  </div>
                }
              ></SubMenu>
              <SubMenu
                title={
                  <div>
                    <Tooltip title="Dashboard Progress" placement="right">
                      <div className="logo1">
                        <Link href="/dashboard/progress">
                          <a className="help-logo">
                            <Progress />
                          </a>
                        </Link>
                      </div>
                    </Tooltip>
                  </div>
                }
              ></SubMenu>
            </div>
            <div className="help">
              <SubMenu
                title={
                  <div>
                    <Tooltip title="Helpdesk" placement="right">
                      <div className="logo2">
                        <a onClick={() => setisOpen(true)} className="help-logo2">
                          <Help />
                        </a>
                        <Modal open={isOpen} className="modal">
                          <div className="flex-logo">
                            <img src={iconwa} alt="" className="png" />
                            <img src={icontele} alt="" className="png" />
                            <img src={iconmsg} alt="" className="png" />
                          </div>
                        </Modal>
                      </div>
                    </Tooltip>
                  </div>
                  
                }
              ></SubMenu>
              <SubMenu
                title={
                  <div>
                    <Tooltip title="Logout" placement="right">
                      <div className="logo1">
                        <a
                          className="power-logo"
                          onClick={() => dispatch(logout())}
                        >
                          <SidebarPower size={25} />
                        </a>
                      </div>
                    </Tooltip>
                  </div>
                }
              ></SubMenu>
            </div>
          </Menu>
        </Scrollbars>
      </Sider>
    </SidebarWrapper>
  );
}
