import OverviewL from "@iso/assets/images/icon/overview.svg";
import Image from "next/image";
import SidebarWrapper from "./Sidebar.styles";
const myLoader = ({ src, width, quality }) => {
  return `https://example.com/${src}?w=${width}&q=${quality || 75}`;
};

const Overview = (props) => {
  return (
    <SidebarWrapper>
      <Image
        loader={myLoader}
        className="overviewicon"
        src={OverviewL}
        alt="Picture of the author"
        layout="fixed"
        width="25"
        height="25"
      />
    </SidebarWrapper>
  );
};

export default Overview;
