import helpL from "@iso/assets/images/icon/help.svg";
import Image from "next/image";
import SidebarWrapper from "./Sidebar.styles";

function Popup() {
  var popup = document.getElementById("myPopup");
  popup.classList.toggle("show");
}
const myLoader = ({ src, width, quality }) => {
  return `https://example.com/${src}?w=${width}&q=${quality || 75}`;
};

const Help = (props) => {
  return (
    <SidebarWrapper>
      <Image
        loader={myLoader}
        className="overviewicon"
        id="mypopup"
        src={helpL}
        alt="Picture of the author"
        layout="fixed"
        width="25"
        height="25"
      />
    </SidebarWrapper>
  );
};
export default Help;
