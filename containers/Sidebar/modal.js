import { formatRelativeTime } from "@formatjs/intl";
import React from "react";
const MODAL_STYLES = {
  position: "relative",
};
export default function Modal({ open, children }) {
  if (!open) return null;
  return <div style={MODAL_STYLES}>{children}</div>;
}
