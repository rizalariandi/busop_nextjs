import ProgressL from "@iso/assets/images/icon/Progress.svg";
import Image from "next/image";
import SidebarWrapper from "./Sidebar.styles";
const myLoader = ({ src, width, quality }) => {
  return `https://example.com/${src}?w=${width}&q=${quality || 75}`;
};

const Progress = (props) => {
  return (
    <SidebarWrapper>
      <Image
        loader={myLoader}
        src={ProgressL}
        className="progress-logo"
        alt="Picture of the author"
        layout="fixed"
        width="25"
        height="25"
      />
    </SidebarWrapper>
  );
};

export default Progress;
