import styled from "styled-components";
import { palette } from "styled-theme";
import { transition, borderRadius } from "@iso/lib/helpers/style_utils";
import WithDirection from "@iso/lib/helpers/rtl";

const SidebarWrapper = styled.div`
  .isomorphicSidebar {
    z-index: 1000;
    background: ${palette("grayscale", 1)};
    width: 280px;
    flex: 0 0 280px;

    .scrollarea {
      height: calc(100vh - 70px);
    }

    @media only screen and (max-width: 767px) {
      width: 240px !important;
      flex: 0 0 240px !important;
    }

    .isoLogoWrapper {
      height: 70px;
      margin: 0;
      padding: 0 10px;
      text-align: center;
      overflow: hidden;
      ${borderRadius()};
      background: ${palette("grayscale", 2)};

      h3 {
        a {
          font-size: 21px;
          font-weight: 300;
          line-height: 70px;
          letter-spacing: 3px;
          text-transform: uppercase;
          color: ${palette("grayscale", 6)};
          display: block;
          text-decoration: none;
        }
      }
    }

    &.ant-layout-sider-collapsed {
      .isoLogoWrapper {
        padding: 0px;

        h3 {
          a {
            font-size: 27px;
            font-weight: 500;
            letter-spacing: 0;
          }
        }
      }
    }

    .isoDashboardMenu {
      height: 60px;
      padding-top: 35px;
      padding-bottom: 35px;
      background: transparent;

      a {
        text-decoration: none;
        font-weight: 400;
      }

      .ant-menu-item {
        width: 100%;
        display: -ms-flexbox;
        display: flex;
        -ms-flex-align: center;
        align-items: center;
        /* padding: 0 24px; */
        margin: 0;
      }

      /* &.ant-menu-dark .ant-menu-item:hover .isoMenuHolder,
      &.ant-menu-dark .ant-menu-submenu-active .isoMenuHolder,
      &.ant-menu-dark .ant-menu-submenu-open .isoMenuHolder,
      &.ant-menu-dark .ant-menu-submenu-selected .isoMenuHolder,
      &.ant-menu-dark .ant-menu-submenu-title:hover .isoMenuHolder,
      &.ant-menu-dark .ant-menu-item-selected > a,
      &.ant-menu-dark .ant-menu-item-selected > span > a,
      &.ant-menu-dark .ant-menu-item-selected > a:hover,
      &.ant-menu-dark .ant-menu-item-selected > span > a:hover {
        color: #fff !important;
      } */

      .nav-text {
        font-size: 14px;
        color: inherit;
        font-weight: 400;

        ${transition()};
      }

      .ant-menu-item-selected {
        .anticon {
          bac
          color: #fffff;
        }
        .help3{
          margin-left:1px;
        }

        svg {
          color: #ffffff;
        }

        .nav-text {
          color: #fff;
        }
      }

      .ant-menu-dark .ant-menu-inline.ant-menu-sub {
        background: ${palette("secondary", 5)};
      }

      .ant-menu-submenu-inline,
      .ant-menu-submenu-vertical {
        > .ant-menu-submenu-title {
          width: 100%;
          display: flex;
          align-items: center;
          /* padding: 0 24px; */

          > span {
            display: flex;
            align-items: center;
          }
          .help-logo {
            color: #ffffff;
          }
           .help-logo2 {
            color: #ffffff;
            border-radius: 8px;
          }
            .help-logo2:hover{
          background-color: rgba(50,52,85,255);
        }
          .ant-menu-submenu-arrow {
            display: none;
            &:before,
            &:after {
              width: 8px;
              ${transition()};
            }

            &:before {
              transform: rotate(-45deg) translateX(3px);
            }

            &:after {
              transform: rotate(45deg) translateX(-3px);
            }

            ${
              "" /* &:after {
            content: '\f123';
            font-family: 'Ionicons' !important;
            font-size: 16px;
            color: inherit;
            left: ${props => (props['data-rtl'] === 'rtl' ? '16px' : 'auto')};
            right: ${props => (props['data-rtl'] === 'rtl' ? 'auto' : '16px')};
            ${transition()};
          } */
            };
          }

          &:hover {
            .ant-menu-submenu-arrow {
              &:before,
              &:after {
                color: #ffffff;
              }
            }
          }
        }
        .overview{
          width: 70%;
        }
        .ant-menu-inline,
        .ant-menu-submenu-vertical {
          font-size: 13px;
          font-weight: 400;
          margin: 0;
          color: inherit;

          &:hover {
            a {
              color: #ffffff !important;
            }
          }
        }

        .ant-menu-item-group {
          padding-left: 0;

          .ant-menu-item-group-title {
            padding-left: 100px !important;
          }
          .ant-menu-item-group-list {
            .ant-menu-item {
              padding-left: 125px !important;
            }
          }
        }
      }
      .help {
        height: 80vh;
        padding-left:1px;
        display: flex;
        flex-direction: column;
        justify-content: end;
      }
      .top-btn{
        display: flex;
        position: absolute;
        flex-direction: column;
        gap:20px;
        padding-left:1px;
        justify-content: center;
        align-items: centr;
      }
      .logo1{
        border-style: none;
        border-radius: 8px;
        width: 30px;
        height: 30px;
        padding-left:2px;
        padding-top:1px;
        align-items: center;
      }
      .logo2{
        border-style: none;
        display:flex;
        flex-direction:row;
        width: 30px;
        height: 30px;
        padding-left:2px;
        padding-top:1px;
        align-items: center;
      }
       .logo1:hover{
          background-color: rgba(50,52,85,255);
        }

        .modal{
          width:28px;
          height:10px;
          border-radius:8px;
        }
        .flex-logo{
          display:flex;
          flex-direction:column;
          gap:4px;
          position: fixed;
          bottom: 66px;
              @media only screen and (min-height: 843px) {
                 bottom: 108px;
    }
           @media only screen and (min-height: 1180px) {
                 bottom: 165px;
    }
        }
        .png{
          width:26px;
          height:21px;
          padding-bottom:1px;
          padding-left:7px;
         
        }
      .power-logo{
        color:white;
      }
      .ant-menu-sub {
        box-shadow: none;
        background-color: transparent !important;
      }
    }

    &.ant-layout-sider-collapsed {
      .nav-text {
        display: none;
      }

      .ant-menu-submenu-inline > {
        .ant-menu-submenu-title:after {
          display: none;
        }
      }

      .ant-menu-submenu-vertical {
        > .ant-menu-submenu-title:after {
          display: none;
        }

        .ant-menu-sub {
          background-color: transparent !important;

          .ant-menu-item {
            height: 35px;
          }
        }
      }
    }
  }
`;

export default WithDirection(SidebarWrapper);
