import Summary from "@iso/assets/images/icon/summary.svg";
import Image from "next/image";
const myLoader = ({ src, width, quality }) => {
  return `https://example.com/${src}?w=${width}&q=${quality || 75}`;
};

const SummaryL = (props) => {
  return (
    <Image
      loader={myLoader}
      src={Summary}
      alt="Picture of the author"
      layout="fixed"
      width="25"
      height="25"
    />
  );
};

export default SummaryL;
