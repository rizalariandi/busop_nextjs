import React, { Component } from "react";
import LayoutContentWrapper from "@iso/components/utility/layoutWrapper";
import LayoutContent from "@iso/components/utility/layoutContent";
import filter from "@iso/assets/images/icon/filter.svg";
import { Row, Col } from "antd";
import basicStyle from "@iso/assets/styles/constants";
import { useDispatch, useSelector } from "react-redux";
import Actions from "@iso/redux/themeSwitcher/actions";
const { switchActivation, changeTheme } = Actions;
export default function () {
  const { rowStyle, colStyle, gutter } = basicStyle;
  const dispatch = useDispatch();
  return (
    <LayoutContentWrapper style={{ height: "100vh" }}>
      <Row style={rowStyle} gutter={gutter} justify="end">
        <button
          type="primary"
          className="switcherToggleBtn"
          onClick={() => {
            dispatch(switchActivation());
          }}
        >
          <img src={filter} alt="bucket" />
        </button>
      </Row>
      <LayoutContent>
        <h1>Blank Page</h1>
      </LayoutContent>
    </LayoutContentWrapper>
  );
}
