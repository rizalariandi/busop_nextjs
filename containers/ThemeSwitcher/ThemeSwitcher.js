import React, { useState, useRef, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Actions from "@iso/redux/themeSwitcher/actions";
import ThemeSwitcherStyle from "./ThemeSwitcher.styles";
import close from "@iso/assets/images/icon/07-icon.svg";
import DatePick from "./Datepicker";
import filter from "@iso/assets/images/icon/filter.svg";
import { useOnClickOutside } from "../Forms/Progress/useOnClickOutside";

import OutsideClickHandler from "react-outside-click-handler";

const { switchActivation, changeTheme } = Actions;
export default function ThemeSwitcher() {
  const { isActivated } = useSelector((state) => state.ThemeSwitcher);
  const dispatch = useDispatch();
  let menuRef = useRef();

  // useEffect(() => {
  //   document.addEventListener("mousedown", (event) => {
  //     if (!menuRef.current?.contains(event.target)) {
  //       ThemeSwitcher(false);
  //     }
  //   });
  // });
  return (
    <ThemeSwitcherStyle className={isActivated ? "isoThemeSwitcher active" : "isoThemeSwitcher"} ref={menuRef} >
      <OutsideClickHandler onOutsideClick={() => {
        if(isActivated){
          dispatch(switchActivation());
        };
      }}>
        <div className="SwitcherBlockWrapper">
          <div className="Tanggal">
            <h2 className="filter">Filter</h2>
            <p>Tanggal</p>
            <div className="p-tanggal">
              <DatePick></DatePick>
            </div>
            <p>Subdir</p>
            <div className="p-tanggal">
              <input type="text" id="p-subdir" name="subdir" />
            </div>
            <p>Layanan</p>
            <div className="p-tanggal">
              <input type="text" id="p-layanan" name="layanan" />
            </div>
            <p>Customer Name</p>
            <div className="p-tanggal">
              <input type="text" id="cname" name="customername" />
            </div>
            <div className="btn-close">
              <button
                type="primary"
                className="btn2"
                onClick={() => {
                  dispatch(switchActivation());
                }}
              >
                <img src={close} alt="bucket" className="png" />
              </button>
            </div>
          </div>
        </div>
      </OutsideClickHandler>
    </ThemeSwitcherStyle>
    
  );
}
