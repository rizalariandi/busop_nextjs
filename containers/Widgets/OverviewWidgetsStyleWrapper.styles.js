import styled from 'styled-components';
import { palette } from 'styled-theme';
import WithDirection from '@iso/lib/helpers/rtl';



const OverviewWidgetsStyleWrapper = styled.div`
  width: 100%;
  padding-bottom:30px;

  
  
`;

export default WithDirection(OverviewWidgetsStyleWrapper);
