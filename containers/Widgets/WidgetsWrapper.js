import styled from 'styled-components';
import { palette } from 'styled-theme';
import WithDirection from '@iso/lib/helpers/rtl';


const CustomWidgetsWrapper = styled.div`
  width: 100%;
  min-height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;
  background-size: cover;

  .isoWidgetsWrapper {
    width: 100%;
    background-color: #ffffff:
  }

`;

export default WithDirection(CustomWidgetsWrapper);
