import React, { Component, lazy } from 'react';
import clone from 'clone';
import { Row, Col } from 'antd';
import LayoutWrapper from '@iso/components/utility/layoutWrapper';
import basicStyle from '@iso/assets/styles/constants';
import IsoWidgetsWrapper from './WidgetsWrapper';
import IsoWidgetBox from './WidgetBox';
import CardWidget from './Card/CardWidget';
import ProgressWidget from './Progress/ProgressWidget';
import { useRouter } from 'next/router';


const Bar = lazy(() => import('@iso/containers/Charts/BusopChart/Components/Bar/Bar'));
const HorizontalBar = lazy(() => import('@iso/containers/Charts/BusopChart/Components/HorizontalBar/HorizontalBar'));
const Doughnut = lazy(() => import('@iso/containers/Charts/BusopChart/Components/Doughnut/Doughnut'));
const Box = lazy(() => import('@iso/components/utility/box'));
const ContentHolder = lazy(() =>
  import('@iso/components/utility/contentHolder')
);
const PageHeader = lazy(() => import('@iso/components/utility/pageHeader'));

export default class extends Component {
  render() {
    const { rowStyle, colStyle, gutter } = basicStyle;
    return (
      <LayoutWrapper className="isoMapPage">
        <PageHeader>React Charts 2</PageHeader>
        
        <Row style={rowStyle} gutter={gutter} justify="start">
          <Col md={12} xs={24} style={colStyle}>
            <Box title="Bar (custom size)">
              <ContentHolder>
                <Bar />
              </ContentHolder>
            </Box>
          </Col>
          <Col md={12} xs={24} style={colStyle}>
            <Box title="Horizontal Bar Example">
              <ContentHolder>
                <HorizontalBar />
              </ContentHolder>
            </Box>
          </Col>
        </Row>
        
      </LayoutWrapper>
    );
  }
}