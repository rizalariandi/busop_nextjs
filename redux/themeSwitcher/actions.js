import { getCurrentTheme } from "./config";
const actions = {
  CHANGE_THEME: "CHANGE_THEME",
  SWITCH_ACTIVATION: "SWITCH_ACTIVATION",
  switchActivation: () => ({
    type: actions.SWITCH_ACTIVATION,
  }),
  changeTheme: (attribute, themeName) => {
    const theme = getCurrentTheme(attribute, themeName);
    if (attribute === "layoutTheme") {
      document.getElementsByClassName(
        "isomorphicContent"
      )[0].style.backgroundColor = theme.backgroundColor;
    }
    const flip_flip_button = document.getElementById("flip-flop");
    flip_flip_button.addEventListener("click", function () {
      flip_flip_button.style.right = 400 + "px";
    });
    return {
      type: actions.CHANGE_THEME,
      attribute,
      theme,
    };
  },
};
export default actions;
