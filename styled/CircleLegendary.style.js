import styled from 'styled-components';
import { palette } from 'styled-theme';
import WithDirection from '@iso/lib/helpers/rtl';


const CircleLegend = styled.div`
    width:25px;
    height:25px;
    border-radius:100px;
    background-color:#ff6384 !important;
    
`;

export default WithDirection(CircleLegend);