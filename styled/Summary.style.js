import React from "react";
import Cards from "@iso/components/uielements/card";
import styled from "styled-components";
import { palette } from "styled-theme";

const CardComp = (props) => <Cards {...props} />;

const Card = styled(CardComp)`
  .ant-card-head {
    background-color: ${palette("grayscale", 0)};
    ${
      "" /* border-bottom: 1px solid ${palette('border', 0)}; */
    } border-bottom: 0;

    .ant-card-head-title {
      color: ${palette("text", 0)};
    }
  }

  .ant-card-extra {
    a {
      color: ${palette("primary", 0)};
      text-decoration: none;

      &:focus {
        text-decoration: none;
      }
    }
  }

  .ant-card-body {
    p {
      color: ${palette("text", 3)};
      line-height: 1.5;
      margin-bottom: 10px;

      &:last-child {
        margin-bottom: 0;
      }
    }
  }

  &.ant-card-bordered {
    border: 0px solid ${palette("border", 0)};
    border-radius: 20px;
    box-shadow: 5px 10px #e5e5e5;
    .ant-card-head {
      border-radius: 20px;
    }

    &:hover {
      border: 1px solid ${palette("border", 0)} !important;
    }
  }

  &.ant-card-loading {
    .ant-card-body {
      p {
        margin-bottom: 0;
      }
    }
  }

  .custom-card {
    padding: 10px 16px;
    h3 {
      color: ${palette("text", 1)};
      font-weight: 500;
    }
    p {
      color: ${palette("grayscale", 0)};
    }
  }
  .text-content {
    display: flex;
    justify-content: space-between;
    flex-direction: row;
    gap: 17px;
  }
  .status-text {
    display: flex;
    justify-content: space-between;
    flex-direction: row;
    gap: 17px;
    .up-text {
      color: #00ff19;
      font-weight: bold;
    }
    .onprogress {
      color: blue;
      font-weight: 700;
    }
    .closed {
      color: #e50000;
      font-weight: 700;
    }
  }

  .text {
    font-size: 17px;
  }
  .btn {
    display: flex;
    justify-content: center;
    align-itmes: center;
    border-radius: 20px;
    background-color: #ffffff;
  }
  }


  .custom-image img {
    display: block;
  }
`;

export default Card;

export const CardIcon = styled.img`
  width: 8px;
  height: 8px;
`;
