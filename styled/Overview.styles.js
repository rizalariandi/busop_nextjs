import styled from 'styled-components';
import { palette } from 'styled-theme';
import WithDirection from '@iso/lib/helpers/rtl';


const OverviewStyleWrapper = styled.div`
    width: 100%;
    min-height: 100vh;
    
    background-color: #ffffff;
    
`;

export default WithDirection(OverviewStyleWrapper);
