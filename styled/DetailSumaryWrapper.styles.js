import styled from 'styled-components';
import { palette } from 'styled-theme';
import WithDirection from '@iso/lib/helpers/rtl';

import bgImage from '@iso/assets/images/bglogin.png';

const DetailSumaryWrapper = styled.div`
    width: 100%;
    min-height: 100vh;
    
    background-color: #ffffff;
    padding-left:20px;
    padding-right:20px;
    padding-bottom:20px;
    .Perusahaan{
        font-size:35px !important;
        font-weight:bold;
        padding-left:10px !important;
        padding-top:20px !important;
        padding-bottom:10px !important;
    }
`;

export default WithDirection(DetailSumaryWrapper);
