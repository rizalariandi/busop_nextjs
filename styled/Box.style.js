import styled from 'styled-components';
import { palette } from 'styled-theme';
import WithDirection from '@iso/lib/helpers/rtl';


const Box = styled.div`
    width: 50px;
    height: 50px;
    background-color: #FFDEE0;
    border-radius: 10px;
    margin-left:30px;
    margin-top:20px;
    margin-bottom:10px;
    
    

    .icon{
        font-size:20px !important;
        padding-top:10px;
    }

`;
export default WithDirection(Box);