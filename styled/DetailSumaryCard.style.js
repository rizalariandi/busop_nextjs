import styled from 'styled-components';
import { palette } from 'styled-theme';
import WithDirection from '@iso/lib/helpers/rtl';


const CardSumary = styled.div`
    width: 100%;
    min-height: 100px;
    background-color: #ffffff;
    border-radius: 5px;
    border: 2px solid #e6e6e6;
    margin-bottom: 15px;
    marign-top: 10px;

   
    .card-title{
        font-size:20px !important;
        font-weight:bold;
        padding-top:20px !important;
    }
    .id{
        font-size:20px !important;
        font-weight:bold;
        color:#8B98A0;
    }
    .icon-phone{
        width:13px;
        height:13px;
    }
`;

export default WithDirection(CardSumary);