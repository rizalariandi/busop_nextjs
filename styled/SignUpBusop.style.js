import styled from 'styled-components';
import { palette } from 'styled-theme';
import WithDirection from '@iso/lib/helpers/rtl';

import bgImage from '@iso/assets/images/bgsignup.png';

const SignUpBusopWrapper = styled.div`
    width: 100%;
    min-height: 100vh;
    
    background-color: #ffffff;
    @media only screen and (max-width: 820px) {
        .website-version {
          display: none !important;
        }
        
    }
    @media only screen and (min-width: 821px) {
        
        .mobile-version {
            display: none !important;
        }
    }
    .LoginContent-left{
        
        background: url(${bgImage}) no-repeat center center !important;
        background-size: cover !important;
        
       
    }
    .assetimageleft{
        
    }
    .extraCustomClass{
        margin-bottom: 0px !important;
        margin-right: 0px !important;
        margin-left: 0px !important;
    }
    .LoginContent-right{
        
        
    }
    .ContentContainer{
       
        text-align: center;

    }
    .content{
       margin-top: 300px;
    }
    .Title{
        
        font-family: 'Roboto', sans-serif;
        textweight: bold;
        font-size: 84px;
        color: #ffffff;
        
    }
    .subtitle{
        
        font-family: 'Roboto', sans-serif;
        textweight: bold;
        font-size: 14px;
        color: #ffffff;
        
    }
    .ButtonContainer{
        margin-left: 20px;
    }

    .login-btn{
        color:#ffffff !important;
        font-family: 'Roboto', sans-serif;
        textweight: bold;
        font-size: 16pt;
        padding-top: 180px;
        cursor: pointer;
    }
    .signup-btn{
        color:#E42312;
        font-family: 'Roboto', sans-serif;
        textweight: bold;
        font-size: 16pt;
       
        padding-top: 45px;
        cursor: pointer;
    }
    .LoginForm{
        
        text-align: center;
        margin-top: 30px !important;
        
    }
    .LoginForm-title{
        font-family: 'Roboto', sans-serif;
        font-weight: bold !important;
        font-size: 35px;
        color:#161E45;
    }
    .isoCustomInput{
        border-radius: 30px !important;
        padding-left: 50px !important;
        padding-right: 50px !important;
        text-align: center;
        width: auto !important;
    }
    .isoInputWrapper{
        margin-bottom: 20px;
        margin-top: 20px;
    }
    .LoginForm-subtitle{
        font-family: 'Roboto', sans-serif;
        font-size: 14px;
        color:#161E45;
    }
    .form-group{
        margin-bottom: 20px;
        margin-top: 20px;
    }
    .logo{
        margin-bottom: 20px;
        margin-top: 20px;
    }
    .LoginForm-header{
        margin-top: 40px;
    }
    .btnLogin{
        background-color: #E42312;
        color: #ffffff !important;
        font-family: 'Roboto', sans-serif;
        border-radius: 30px !important;
        padding-left: 50px !important;
        padding-right: 50px !important;
        cursor: pointer;
    }
    .iconWrapper{
        margin-top: 20px;
    }
    .icon{
        margin-right: 8px;
        margin-left: 8px;
        color: #E42312;
        cursor: pointer;
    }
    .button-tab-mobile{
        margin-top: 30px;
        text-align: center;
        
    }
    .btnLog{
        background-color: #161E45;
        color: #E42312 !important;
        margin-left: 10px;
        margin-right: 10px;
        border-radius: 30px !important;
        font-weight: bold !important;
        font-family: 'Roboto', sans-serif;
        padding-left: 20px !important;
        padding-right: 20px !important;
        padding-top: 10px !important;
        padding-bottom: 10px !important;
        cursor: pointer;
    }
    .btnSign{
        margin-left: 10px;
        margin-right: 10px;
        border-radius: 30px !important;
        font-weight: bold !important;
        font-family: 'Roboto', sans-serif;
        padding-left: 20px !important;
        padding-right: 20px !important;
        padding-top: 10px !important;
        padding-bottom: 10px !important;
        cursor: pointer;
    }
`;

export default WithDirection(SignUpBusopWrapper);
